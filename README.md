# hello-node-js

This is a simple Node.js Web Application to explain **port mapping** with Docker.

- 👋 show the Dockerfile 🐳

```bash
docker build -t hello .
docker run -d -p 6060:8080 --name hello-container --rm hello
# I'm listening on the 8080 http port, I map the host 6060 port of the host on the container port
```
> - Open http://localhost:8080 `KO`
> - Open http://localhost:6060 `OK`

## Second part of the demo with the GUI

- Go to the Dashboard:
  - show container (Logs)
  - then Exec
    - ls
    - cat index.js
  - then Files

```bash
docker stop hello-container
```